import {
  initAdminUsersApp,
  initAdminUsersFilterApp,
  initDeleteUserModals,
  initAdminUserActions,
} from '~/admin/users';
import initConfirmModal from '~/confirm_modal';

initAdminUsersApp();
initAdminUsersFilterApp();
initAdminUserActions();
initDeleteUserModals();
initConfirmModal();
